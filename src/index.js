import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, createStore} from "redux";
import Login from './components/Login'
import Register from './components/Register'
import Chat from './components/Chat'
import reducer from "./reducers/rootReducer";
import GroupChat from './components/GroupChat'
import thunk from "redux-thunk";
import {Provider} from "react-redux"
import Cookies from 'universal-cookie';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import UsersToChat from "./components/UsersToChat";
const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk));
const cookies = new Cookies();
let eventTypes =  require('./config/eventTypes');
if (cookies.get('token')) {
    store.dispatch({
        type:eventTypes.authenticated,
        token:cookies.get('token')
    })
    store.dispatch({
        type:eventTypes.userFetched,
        user:cookies.get('user')
    })
}
ReactDOM.render(
<Provider store={store}>
    <BrowserRouter>
    <Switch>
    <Route exact path="/" component={Login} />
    <Route exact path="/register" component={Register} />
    <Route exact path="/chat" component={Chat} />
    <Route exact path="/usersToChat" component={UsersToChat} />
    <Route exact path="/groupChat" component={GroupChat} />

    </Switch>
</BrowserRouter>

</Provider>, document.getElementById('root'));
