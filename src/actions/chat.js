import axios from "axios";
import {API_ROOT} from "../config/config";
import {createChatUiMessage} from '../util/createReactChatMessage'
import client from '../util/client'
let eventTypes = require('../config/eventTypes');


export function getMessagesForConversationId(conversationId) {
    return dispatch => {
        client().get(`${API_ROOT}/conversation/messages/get`, {params: {
            conversation_id: conversationId
            }}).then(response => {
            dispatch({
                type:eventTypes.fetchMessagesForConversation,
                messages: response.data
            })
        }).then(error => {
            console.log(error)
        })
    }
}

export function newMessage(message) {
    return dispatch => {
        client().post(`${API_ROOT}/message`, message).then(response => {
            dispatch({
                type:eventTypes.messageSaved,
                message:response.data.message,
                saved: response.data.messageSaved
            });
        }).then(error => {
            console.log(error)
        })
    }
}

export function createConversationId(receiver_id) {
    return dispatch => {
        client().post(`${API_ROOT}/conversation/create`, {
            receiver_id: receiver_id
            }).then(response => {
            dispatch({
                type:eventTypes.createConversationId,
                conversation_id: response.data.conversation.id,
                conversation:response.data.conversation,
                receiver_id:response.data.receiver_id
            });
        }).then(error => {
            console.log(error)
        })
    }
}

export function newGroupChatMessage(message) {
    return dispatch => {
        client().post(`${API_ROOT}/groupMessage/new/message`, {
            message: message
        }).then(response => {
            dispatch({
                type:eventTypes.newGroupMessage,
                message:response.data.message
            });
        }).then(error => {
            console.log(error)
        })
    }
}

export function userIsTyping(conversation_id) {
    return dispatch => {
        client().post(`${API_ROOT}/conversation/userIsTyping`, {conversation_id:conversation_id}).then(response => {

        }).then(error => {
            console.log(error)
        })
    }
}

export function groupMessages() {
    return dispatch => {
        client().get(`${API_ROOT}/groupMessage`).then(response => {
            dispatch({
                type:eventTypes.fetchGroupMessages,
                messages:response.data
            });
        }).then(error => {
            console.log(error)
        })
    }
}




