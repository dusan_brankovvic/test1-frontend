import axios from 'axios'
import {API_ROOT} from "../config/config";
import Cookies from 'universal-cookie';
import client from '../util/client'
let eventTypes = require('../config/eventTypes');

const cookies = new Cookies();

export function authenticate(data) {
  return dispatch => {
    axios.post(`${API_ROOT}/authenticate`, data)
      .then(response => {
        cookies.set('token', response.data.token, { path: '/' });
        cookies.set('user', response.data.user, { path: '/' });
        dispatch({
          type:eventTypes.authenticated,
          token:response.data.token,
          username: response.data.user.name,
        });
        window.location = 'http://localhost:3000/usersToChat'
      }).catch((error) => {
      if (error.response.status === 401) {
        dispatch({
          type:eventTypes.loginError,
          data:error.response.data
        });
      }
    })

  }
}

export function getUsersToChat() {
  return dispatch => {
    client().get(`${API_ROOT}/user`)
        .then(response => {
          dispatch({
            type:eventTypes.usersToChat,
            users:response.data.data,
          });
        }).catch((error) => {
          console.log(error)
    })
  }
}

export function logout(){
  return dispatch => {
    client().post(`${API_ROOT}/logout`).then(response => {
      cookies.remove('token');
      cookies.remove('username');
      dispatch({
        type:eventTypes.logoutUser,
        data:response.data
      });

    })
  }
}

