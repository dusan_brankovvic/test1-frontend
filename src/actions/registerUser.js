import axios from 'axios'
import {API_ROOT} from "../config/config";
import Cookies from 'universal-cookie';
const cookies = new Cookies();
let eventTypes = require('../config/eventTypes');


export function sendRegister(data) {
    return dispatch => {
        axios.post(`${API_ROOT}/register`, data).then(response => {
            cookies.set('token', response.data.token, { path: '/' });
            cookies.set('user', response.data.user, { path: '/' });
            dispatch({
                type:eventTypes.sendRegistrationData,
                data:response.data
            });
        }).then(error => {
            console.log(error)
        })
    }
}


