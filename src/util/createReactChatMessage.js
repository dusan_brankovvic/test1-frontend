import { Message } from 'react-chat-ui'
export function createChatUiMessages (messages, authenticatedUserId) {
    let chatMessage = [];
    for(let i=0;i<messages.length;i++) {
        chatMessage.push(new Message({
            id: (authenticatedUserId === messages[i].sender_id) ? 0 : 1,
            message:messages[i].message
        }))
    }
    return chatMessage;
}

export function createChatUiMessage (message, authenticatedUserId) {
    return new Message({
            id: (authenticatedUserId === message.sender_id) ? 0 : 1,
            message:message.message
        })
    }


