import axios from 'axios'
import Cookies from 'universal-cookie';

let config = require('../config/config');

const cookies = new Cookies();

export default () => {
  let ax = axios.create({
    baseURL: config.API_ROOT,
    headers: {'Authorization': 'Bearer ' + cookies.get('token')}
  });

  ax.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    if (401 === error.response.status) {
      window.location = "/";
      console.log('you must be logged in');
    }
    if (500 === error.response.status) {
      window.location = "/";
    }
  });

  return ax;
}
