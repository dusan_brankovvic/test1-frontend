module.exports = {
  loading:'LOADING_IN_PROGRESS',
  loadingCompleted:'LOADING_COMPLETED',
  sendRegistrationData: 'SEND_REGISTRATION_DATA',
  registerConfirmed: 'REGISTER_CONFIRMED',
  showAllUsers: 'SHOW_ALL_USERS',
  loginUser: 'LOGIN_USER',
  logoutUser: 'LOGOUT_USER',
  emailCheck:'EMAIL_CHECK',
  authenticated:'AUTHENTICATION_COMPLETED',
  loginError: 'LOGIN_ERROR',
  userFetched:'USER_FETCHED',
  fetchMessagesForConversation:'FETCH_MESSAGES_FOR_CONVERSATION',
  messageSaved: 'MESSAGE_SAVED',
  usersToChat:'USERS_TO_CHAT',
  createConversationId:'CREATE_CONVERSATION_ID',
  newGroupMessage:'NEW_GROUP_MESSAGE',
  fetchGroupMessages:'FETCH_GROUP_MESSAGES'
};
