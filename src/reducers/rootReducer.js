import { combineReducers } from "redux"
import userReducer from './userReducer'
import loginReducer from './loginReducer'
import registerReducer from './registerReducer'
import chatReducer from './chatReducer'
import groupReducer from './groupChatReducer'
const rootReducer = combineReducers({
    loginReducer,
    userReducer,
    registerReducer,
    chatReducer,
    groupReducer
});

export default rootReducer;
