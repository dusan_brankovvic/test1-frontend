let eventTypes = require('../config/eventTypes');

export default function reducer(state = {
    data: [],
    messages: [],
    messageSaved: false,
    error:null,
    message:{},
    chatUiMessage: null,
    chatUiMessages:[],
    conversation_id: null,
    conversation: {},
    loading:true,
    receiver_id:null
}, action) {
    switch (action.type) {
        case eventTypes.fetchMessagesForConversation: {
            return {...state, data:action.data,  messages:action.messages, loading:false}
        }
        case eventTypes.messageSaved: {
            return {...state, message:action.message, chatUiMessage:action.chatUiMessage, messageSaved:action.messageSaved, error: action.error, loading:false}
        }
        case eventTypes.createConversationId: {
            return {...state, conversation_id:action.conversation_id, conversation: action.conversation, receiver_id: action.receiver_id, loading:false}
        }
        default: {
            return state
        }
    }
};
