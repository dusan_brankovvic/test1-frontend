let eventTypes = require('../config/eventTypes');

export default function reducer(state = {
    data:[],
    isAuthenticated: false
}, action) {
    switch (action.type) {
        case eventTypes.authenticated: {
            return {...state, token:action.token, isAuthenticated:true, checkError: null,
                username: action.username}
        }
        case eventTypes.logoutUser: {
            return {...state, token:null, isAuthenticated:false, data:action.data}
        }
        case eventTypes.loginError: {
            return {...state, checkError:action.data.error}
        }
        default: {
            return state
        }
    }
};
