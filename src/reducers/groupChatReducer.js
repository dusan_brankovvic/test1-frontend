let eventTypes = require('../config/eventTypes');

export default function reducer(state = {
    user:{},
    messages:[],
    loading:true
}, action) {
    switch (action.type) {
        case eventTypes.fetchGroupMessages: {
            return {...state, messages:action.messages, loading: false}
        }
        default: {
            return state
        }
    }
};
