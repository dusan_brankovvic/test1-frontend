let eventTypes = require('../config/eventTypes');

export default function reducer(state = {
    token:null,
    isAuthenticated:false,
    emailCheck:null,
    data: null,
    step1register: false,
    step2register: false
}, action) {
    switch (action.type) {
        case eventTypes.sendRegistrationData: {
            return {...state, data:action.data, step1register: true}
        }
        case eventTypes.registerConfirmed: {
            return {...state, data:action.data, step1register: false, step2register: true}
        }
        case eventTypes.emailCheck: {
            return {...state, emailCheck:action.data.email}
        }
        default: {
            return state
        }
    }
};

