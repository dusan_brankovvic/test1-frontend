let eventTypes = require('../config/eventTypes');

export default function reducer(state = {
    user:{},
    usersToChat:[],
    loading:true
}, action) {
    switch (action.type) {
        case eventTypes.userFetched: {
            return {...state, user:action.user}
        }
        case eventTypes.usersToChat: {
            return {...state, usersToChat: action.users, loading:false}
        }
        default: {
            return state
        }
    }
};
