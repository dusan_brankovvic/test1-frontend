import { ChatFeed, Message } from 'react-chat-ui'
import React , { Component } from 'react';
import Echo from 'laravel-echo'
import {connect} from "react-redux";
import {getMessagesForConversationId} from "../actions/chat";
import {ClipLoader} from "react-spinners";
import {createChatUiMessages, createChatUiMessage} from "../util/createReactChatMessage";
import {newMessage, userIsTyping} from "../actions/chat";

window.io = require('socket.io-client');
const mapStateToProps = (state) => {
    return {
        token : state.loginReducer.token,
        chatUiMessages: state.chatReducer.chatUiMessages,
        loading: state.chatReducer.loading,
        user: state.userReducer.user,
        messages:state.chatReducer.messages,
        receiver_id: state.chatReducer.receiver_id,
        conversation: state.chatReducer.conversation
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMessagesForConversationId: (conversation_id, userId) => dispatch(getMessagesForConversationId(conversation_id, userId)),
        newMessage: (message) => dispatch(newMessage(message)),
        userIsTyping: (conversation_id) =>dispatch(userIsTyping(conversation_id))
    }
};

class Chat extends Component {
    constructor(props) {
        super(props);
        let echo = new Echo({
            broadcaster: 'socket.io',
            host: 'http://127.0.0.1:6001',
            auth: {
                headers: {
                    'Authorization': 'Bearer ' + this.props.token,
                },
            },
        });
        this.state = {
            message:'',
            messages: [
                new Message({
                    id: 1,
                    message: "I'm the recipient! (The person you're talking to)",
                }), // Gray bubble
                new Message({ id: 0, message: "I'm you -- the blue bubble!" }), // Blue bubble
                new Message({ id: 1, message: "I'm you -- the blue bubble! 2" }), // Blue bubble
            ],
            is_typing:false,
            echo: echo
    };
        let localThis = this;
        echo.channel('laravel_database_private-conversation.' + this.props.conversation.id )
            .listen('NewMessage', (e) => {
                console.log(e);
                let messages = localThis.state.messages;
                let newMessage = createChatUiMessage(e.message, localThis.props.user.id)
                messages.push(newMessage)
                localThis.setState({
                    messages:messages
                })
        })
        echo.channel('laravel_database_private-conversation.' + this.props.conversation.id )
            .listen('UserIsTyping', (e) => {
                console.log(e);
                if(localThis.props.user.id !== e.userId) {
                    this.setState({
                        is_typing:true
                    })
                }
            })
        this.props.getMessagesForConversationId(this.props.conversation.id);
        console.log('token', this.props.token);
        this.sendMessage = this.sendMessage.bind(this);
        this.userIsTyping = this.userIsTyping.bind(this);
    }
    userIsTyping() {
        this.props.userIsTyping(this.props.conversation.id);
        setInterval(function() {
            this.setState({is_typing: false})
        }.bind(this), 3000);
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.messages !== this.props.messages) {
            this.setState({
                messages: createChatUiMessages(nextProps.messages, this.props.user.id)
            })
        }


    }

    sendMessage() {
        this.props.newMessage({
            'sender_id':this.props.user.id,
            'receiver_id':this.props.conversation.receiver_id,
            'conversation_id':this.props.conversation.id,
            'message':this.state.message
        });
        this.setState({
            message:''
        });
    }

    render() {
        return (
            <div>
                <ClipLoader
                    sizeUnit={"px"}
                    size={50}
                    color={'#123abc'}
                    loading={this.props.loading}
                />
            <ChatFeed
                messages={this.state.messages} // Boolean: list of message objects
                isTyping={this.state.is_typing} // Boolean: is the recipient typing
                hasInputField={false} // Boolean: use our input, or use your own
                showSenderName // show the name of the user who sent the message
                bubblesCentered={false} //Boolean should the bubbles be centered in the feed?
                // JSON: Custom bubble styles
                bubbleStyles={
                    {
                        text: {
                            fontSize: 30
                        },
                        chatbubble: {
                            borderRadius: 70,
                            padding: 40
                        }
                    }
                }
            />
            <input type="text" onKeyUp={this.userIsTyping} value={this.state.message} onChange={(e) => {this.setState({'message': e.target.value})}}/>
            <button onClick={this.sendMessage}>Send Message</button>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
