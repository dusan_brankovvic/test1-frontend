import React , { Component } from 'react';
import {sendRegister} from "../actions/registerUser";
import {connect} from "react-redux";
import { ClipLoader } from 'react-spinners';
const mapDispatchToProps = (dispatch) => {
    return {
        registerUser: (data) => dispatch(sendRegister(data)),
    }
};

const mapStateToProps = (state) => {
    return {
        register:state.registerReducer.register,
        authenticated : state.loginReducer.isAuthenticated,
    };
};

class Register extends Component {

    constructor(props){
        super(props);

        this.state = {
            name: "",
            email: "",
            password: "",
            password_confirmation: "",
            isVerified: false,
            loading: false
        }

        this.onChange = this.onChange.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePass = this.onChangePass.bind(this);
        this.onChangePassConfirm = this.onChangePassConfirm.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.register === true) {
            this.setState({
                loading: false
            });

            window.location.href = '/'
        }
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }


    onChangeEmail(e) {
        let dropdown = document.getElementById("emailError");
        dropdown.style.display = 'none';

        this.setState({ email: e.target.value});
    }



    onChangePass(e) {
        let dropdown = document.getElementById("passError");
        dropdown.style.display = 'none';
        let mindropdown = document.getElementById("minpassError");
        mindropdown.style.display = 'none';

        this.setState({ password: e.target.value});
    }

    onChangePassConfirm(e) {
        let dropdown = document.getElementById("passError");
        dropdown.style.display = 'none';
        let mindropdown = document.getElementById("minpassError");
        mindropdown.style.display = 'none';

        this.setState({ password_confirmation: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.state.password !== this.state.password_confirmation){
            let dropdown = document.getElementById("passError");
            dropdown.style.display = 'block';
            return;
        }
        if(this.state.password.length<6){
            let dropdown = document.getElementById("minpassError");
            dropdown.style.display = 'block';
            return;
        }
        this.props.registerUser(this.state);
        this.setState({
            loading: true
        });
    }

    render(){
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <div className="registerbody">
                        <div className="register-head-cont">
                            <div className="register-head">
                                <div className="register-title">Register</div>
                            </div>
                        </div>
                        <div className="loaderReg">
                            <ClipLoader
                                sizeUnit={"px"}
                                size={50}
                                color={'#123abc'}
                                loading={this.state.loading}
                            />
                        </div>
                        <div className="inputbody">
                            <div className="itemfname">
                                <input type="text" name="username" placeholder="User Name"
                                       onChange={this.onChange}   required/>
                            </div>
                            <div className="itememail">
                                <input type="email" name="email" placeholder="Email"
                                       onChange={this.onChangeEmail}   required/><span id="emailError" style={{color: 'red', display: 'none'}} >
                                Email already exists!</span>
                            </div>
                            <div className="itempassword">
                                <input type="password" name="password" placeholder="Password"
                                       onChange={this.onChangePass}   required/>
                            </div>
                            <div className="itempassword2">
                                <input type="password" name="password_confirmation" placeholder="Confirm Password"
                                       onChange={this.onChangePassConfirm}  required/><span id="passError" style={{color: 'red', display: 'none'}} >
                                Password does not match!</span>
                                <span id="minpassError" style={{color: 'red', display: 'none'}} >
                                Password must contain at least 6 or more characters!</span>
                            </div>

                        </div>
                        <div className="registerbutton">
                            <input className="register" type="submit" value="REGISTER"/>
                        </div>

                    </div>
                </form>

            </div>
        );
    }

}
export default connect(mapStateToProps, mapDispatchToProps)(Register);


