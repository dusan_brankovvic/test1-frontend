import { ChatFeed, Message } from 'react-chat-ui'
import React , { Component } from 'react';
import Echo from "laravel-echo";
import {createChatUiMessage, createChatUiMessages} from "../util/createReactChatMessage";
import {ClipLoader} from "react-spinners";
import {groupMessages, newGroupChatMessage} from "../actions/chat";
import {connect} from "react-redux";
window.io = require('socket.io-client');

const mapStateToProps = (state) => {
    return {
        loading : state.groupReducer.loading,
        messages: state.groupReducer.messages,
        user: state.userReducer.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        groupMessages: () => dispatch(groupMessages()),
        newGroupMessage: (message) => dispatch(newGroupChatMessage(message))
    }
};
class GroupChat extends Component {
    constructor(props) {
        super(props);
        this.props.groupMessages();

        let echo = new Echo({
            broadcaster: 'socket.io',
            host: 'http://127.0.0.1:6001',
            auth: {
                headers: {
                    'Authorization': 'Bearer ' + this.props.token,
                },
            },
        });
        this.state = {
            messages:[],
            message:''
        }
        let localThis = this;

        echo.channel('laravel_database_group')
            .listen('GroupEvent', (e) => {
                console.log('a');
                let messages = localThis.state.messages;
                let newMessage = createChatUiMessage(e.message, localThis.props.user.id);
                messages.push(newMessage)
                localThis.setState({
                    messages:messages
                })
            })
        this.sendMessage = this.sendMessage.bind(this);
    }
    sendMessage() {
        this.props.newGroupMessage(this.state.message)
        this.setState({
            message:''
        })
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.messages !== this.props.messages) {
            this.setState({
                messages: createChatUiMessages(nextProps.messages, this.props.user.id)
            })
        }


    }
    render() {
        return (
            <div>
                <ClipLoader
                    sizeUnit={"px"}
                    size={50}
                    color={'#123abc'}
                    loading={this.props.loading}
                />
                <ChatFeed
                    messages={this.state.messages} // Boolean: list of message objects
                    isTyping={this.state.is_typing} // Boolean: is the recipient typing
                    hasInputField={false} // Boolean: use our input, or use your own
                    showSenderName // show the name of the user who sent the message
                    bubblesCentered={false} //Boolean should the bubbles be centered in the feed?
                    // JSON: Custom bubble styles
                    bubbleStyles={
                        {
                            text: {
                                fontSize: 30
                            },
                            chatbubble: {
                                borderRadius: 70,
                                padding: 40
                            }
                        }
                    }
                />
                <input type="text" value={this.state.message} onChange={(e) => {this.setState({'message': e.target.value})}}/>
                <button onClick={this.sendMessage}>Send Message</button>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupChat)
