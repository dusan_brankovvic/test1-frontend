import React, {Component} from 'react';
import {connect} from "react-redux";
import {authenticate} from "../actions/user";
import { ClipLoader } from 'react-spinners';
const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => dispatch(authenticate(data)),
    }
};

const mapStateToProps = (state) => {
    return {
        authenticated : state.loginReducer.isAuthenticated,
    };
};

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            loading: false
        };

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePass = this.onChangePass.bind(this);
        this.submitLogin = this.submitLogin.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.authenticated === false && nextProps.error !== null) {
            let dropdown = document.getElementById("error");
            dropdown.style.display = 'block';
            this.setState({
                loading: false
            });
        }
    }

    onChangeEmail(e) {
        let dropdown = document.getElementById("error");
        dropdown.style.display = 'none';
        this.setState({email: e.target.value});
    }

    onChangePass(e) {
        let dropdown = document.getElementById("error");
        dropdown.style.display = 'none';
        this.setState({password: e.target.value});
    }

    submitLogin(e) {
        e.preventDefault();
        this.props.login(this.state);
        this.setState({
            loading: true
        });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.submitLogin}>
                    <div className="loginbody">
                        <div className="login-head-cont">
                            <div className="login-head">
                                <div className="login-title">Log In</div>
                            </div>
                        </div>
                        <div className="loaderLog">
                            <ClipLoader
                                sizeUnit={"px"}
                                size={50}
                                color={'#123abc'}
                                loading={this.state.loading}
                            />
                        </div>
                        <div className="logininput">
                            <div className="loginemail">
                                <input type="email" value={this.state.email} name="email" placeholder="Email"
                                       onChange={this.onChangeEmail} required/>
                            </div>
                            <div className="loginpassword">
                                <input type="password" value={this.state.password} name="password"
                                       placeholder="Password"
                                       onChange={this.onChangePass} required/>
                            </div>
                            <div id="error" style={{display: 'none'}}>
                                Wrong email or password!
                            </div>
                        </div>
                        <div className="loginbutton">
                            <input className="login" type="submit" value="LOG IN"
                            />
                        </div>

                    </div>
                </form>
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
