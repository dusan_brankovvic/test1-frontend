import React , { Component } from 'react';
import {getUsersToChat} from "../actions/user";
import {createConversationId} from "../actions/chat";
import {connect} from "react-redux";
import {ClipLoader} from "react-spinners";
import {Link} from "react-router-dom";

const mapDispatchToProps = (dispatch) => {
    return {
        getUsersToChat: () => dispatch(getUsersToChat()),
        createConversationId: (receiverId) => dispatch(createConversationId(receiverId))
    }
};

const mapStateToProps = (state) => {
    return {
        usersToChat : state.userReducer.usersToChat,
        loading: state.userReducer.loading,
        conversation_id: state.chatReducer.conversation_id
    };
};
class UsersToChat extends Component {
    constructor(props) {
        super(props);
        this.props.getUsersToChat();
        this.createConversationId = this.createConversationId.bind(this)
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.conversation_id !== this.props.conversation_id) {
            this.props.history.push('chat');
        }
    }

    createConversationId(receiverId) {
        this.props.createConversationId(receiverId)
    }

    render() {
        return(
            <div>
            <ClipLoader
                sizeUnit={"px"}
                size={50}
                color={'#123abc'}
                loading={this.props.loading}
            />
                <ul>
                    {this.props.usersToChat.map((user, index) => {
                        return (<li onClick={() => this.createConversationId(user.id)} key={index}>{user.username}</li>)
                    })}
                </ul>
                <Link to={'/groupChat'}>Group Chat</Link>
            </div>
            )
        }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersToChat)
